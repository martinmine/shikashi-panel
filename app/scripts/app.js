'use strict';

/**
 * @ngdoc overview
 * @name shikashiApp
 * @description
 * # shikashiApp
 *
 * Main module of the application.
 */
angular
  .module('shikashiApp', [
    'ngResource',
    'ngRoute',
    'webStorageModule'
  ])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider.html

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        title: 'Shikashi: Index'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        title: 'Shikashi: Login'
      })
      .when('/register' , {
        templateUrl: 'views/register.html',
        controller: 'RegisterCtrl',
        title: 'Shikashi: Register'
      })
      .when('/dashboard' , {
        templateUrl: 'views/dashboard.html',
        controller: 'PanelCtrl',
        title: 'Shikashi: Dashboard'
      })
      .when('/dashboard/password' , {
        templateUrl: 'views/changepassword.html',
        controller: 'ChangePasswordCtrl',
        title: 'Shikashi: Change Password'
      })
      .when('/error/:code' , {
        templateUrl: 'views/error.html',
        controller: 'ErrorCtrl',
        title: 'Shikashi: Error'
      })
      .when('/logout', {
        templateUrl: 'views/login.html',
        controller: 'LogoutCtrl'
      })
      .otherwise({
        redirectTo: '/error/404'
      });

    $locationProvider.html5Mode(true);
  })
  .constant('RESOURCE', {
    URL: 'https://api.shikashi.me/'
  })
  .run(['$rootScope', '$route', function($rootScope, $route) {
    $rootScope.$on('$routeChangeSuccess', function() {
      document.title = $route.current.title;
    });
  }])
  .filter('offset', function() {
    return function(input, start) {
      start = parseInt(start, 10);
      return input.slice(start);
    };
  });
