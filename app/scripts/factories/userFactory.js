'use strict';

angular.module('shikashiApp')
  .factory('userFactory', ['userService', 'sessionFactory',
    function(userService, sessionFactory) {
      var registerUser = function(email, password, inviteKey, onSuccess, onError) {
        userService.register({}, $.param({
          email: email,
          password: password,
          inviteKey: inviteKey
        }))
          .$promise
          .then(onSuccess)
          .catch(onError);
      };

      var tryLogin = function(email, password, onSuccess, onError) {
        userService.login({}, $.param({
          email: email,
          password: password
        }))
          .$promise
          .then(onSuccess)
          .catch(onError);
      };

      var setPassword = function(currentPassword, newPassword, onSuccess, onError) {
        userService.newPassword($.param({
          currentPassword: currentPassword,
          newPassword: newPassword
        }))
          .$promise
          .then(onSuccess)
          .catch(onError);
      };

      return {
        registerUser: registerUser,
        tryLogin: tryLogin,
        setPassword: setPassword
      };
    }]);
