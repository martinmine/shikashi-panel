'use strict';

angular.module('shikashiApp')
  .factory('sessionFactory', ['webStorage', '$timeout', '$location',
    function(webStorage, timeout, location) {
      var getSessionKey = function() {
        return webStorage.session.get('authKey');
      };

      var setSessionKey = function(key) {
        webStorage.session.remove('authKey');
        webStorage.session.add('authKey', key);
      };

      var getExpirationTime = function() {
        return webStorage.session.get('expTime');
      };

      var setExpirationTime = function(time) {
        webStorage.session.remove('expTime');
        webStorage.session.add('expTime', time);
      };

      var validSession = function() {
        var now = Math.floor(Date.now() / 1000);
        if (getExpirationTime() > 0 && now > getExpirationTime()) {
          return false;
        }

        if (getSessionKey() !== null && getSessionKey() != '') {
          return true;
        }

        return false;
      };

      var redirect = function(url, forceReload) {
        if (forceReload) {
          window.location.href = url;
        } else {

          // AngularJS location provider.
          // Input is module name eg. /settings.
          timeout(function() {
            location.path(url);
          })
        }
      };

      var logout = function() {
        webStorage.session.clear();
      };

      return {
        getSessionKey: getSessionKey,
        setSessionKey: setSessionKey,
        getExpirationTime: getExpirationTime,
        setExpirationTime: setExpirationTime,
        hasValidSession: validSession,
        redirectUser: redirect,
        logout: logout
      };
    }]);
