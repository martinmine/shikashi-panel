'use strict';

angular.module('shikashiApp')
  .factory('fileFactory', ['fileService', 'sessionFactory',
    function(fileService, sessionFactory) {
      var getUploads = function(onSuccess) {
        fileService.uploads()
          .$promise
          .then(onSuccess);
      };

      var deleteUpload = function(uploadId, onSuccess) {
        fileService.deleteUpload({uploadId: uploadId})
          .$promise
          .then(onSuccess);
      };

      return {
        getUploads: getUploads,
        deleteUpload: deleteUpload
      };
    }]);
