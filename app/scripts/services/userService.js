'use strict';

angular.module('shikashiApp')
  .factory('userService', ['$resource', 'RESOURCE', 'sessionFactory', function(resource, RESOURCE, sessionFactory) {
    return resource('', {}, {
      register: {
        method: 'POST',
        url: RESOURCE.URL + 'register',
        isArray: false,
        paramDefaults: {token: '@token'},
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      },

      login: {
        method: 'POST',
        url: RESOURCE.URL + 'login',
        isArray: false,
        paramDefaults: {token: '@token'},
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      },

      newPassword: {
        method: 'POST',
        url: RESOURCE.URL + 'account/password',
        isArray: false,
        headers: {'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': sessionFactory.getSessionKey()}
      }
    });
  }]);
