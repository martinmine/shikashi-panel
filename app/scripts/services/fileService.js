'use strict';

angular.module('shikashiApp')
  .factory('fileService', ['$resource', 'sessionFactory', 'RESOURCE', function(resource, sessionFactory, RESOURCE) {
    return resource('', {}, {

      /**
       * Gets the list of uploads that a user has made.
       */
      uploads: {
        method: 'GET',
        url: RESOURCE.URL + 'account/uploads',
        isArray: true,
        headers: {'Authorization': sessionFactory.getSessionKey()}
      },

      deleteUpload: {
        method: 'DELETE',
        url: RESOURCE.URL + ':uploadId/delete',
        isArray: true,
        paramDefaults: {uploadId: '@uploadId'},
        headers: {'Authorization': sessionFactory.getSessionKey()}
      },

      /**
       * Resource for uploading a file.
       */
      newUpload: {
        method: 'POST',
        url: RESOURCE.URL + 'upload',
        isArray: true,
        headers: {'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': sessionFactory.getSessionKey()} // ???
      }
    });
  }]);
