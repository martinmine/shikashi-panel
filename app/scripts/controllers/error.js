/**
 * Created by marti_000 on 11.06.2015.
 */
angular.module('shikashiApp')
  .controller('ErrorCtrl', function ($scope, $routeParams) {
    if ($routeParams.code == '500') {
      $scope.message = 'Internal server error';

    } else if ($routeParams.code == '403') {
      $scope.message = "You're not allowed in my bedroom."
    } else {
      $routeParams.code = 404;
      $scope.message = 'Not found';
    }
    $scope.status = $routeParams.code;
  });
