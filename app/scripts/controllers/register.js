/**
 * Created by marti_000 on 11.06.2015.
 */
angular.module('shikashiApp')
  .controller('RegisterCtrl', ['$scope', 'userFactory', 'sessionFactory', function (scope, userFactory, sessionFactory) {
    scope.register = function(email, password, passwordConfirm, key) {
      if (passwordConfirm != password) {
        scope.message = 'Passwords does not match';
        return;
      }

      if (password.length < 5) {
        scope.message = 'Password too short (min 5 characters)';
        return;
      }

      userFactory.registerUser(email, password, key, function(result) {
        userFactory.tryLogin(email, password, function(response) {
          sessionFactory.setSessionKey(response.key);
          sessionFactory.setExpirationTime(response.expirationTime);
          sessionFactory.redirectUser('/dashboard', false);
        }, function(error) {
          scope.message = 'Something went really wrong';
        });
      }, function(error) {
        switch (error.status) {
          case 400: scope.message = 'Missing fields'; break;
          case 403: scope.message = 'Your invite key is invalid'; break;
          case 406: scope.message = 'Password is too weak'; break;
          case 409: scope.message = 'User already exists with this email'; break;
          default: scope.message = 'Unknown server error'; break;
        }
      });
    }
  }]);
