/**
 * Created by marti_000 on 11.06.2015.
 */
angular.module('shikashiApp')
  .controller('LogoutCtrl', ['$scope', 'sessionFactory', function (scope, sessionFactory) {
    sessionFactory.logout();
    sessionFactory.redirectUser('/login', false);
  }]);
