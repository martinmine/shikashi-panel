'use strict';

/**
 * @ngdoc function
 * @name shikashiApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the shikashiApp
 */
angular.module('shikashiApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
