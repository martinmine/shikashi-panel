'use strict';

/**
 * @ngdoc function
 * @name shikashiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the shikashiApp
 */
angular.module('shikashiApp')
  .controller('MainCtrl', ['$scope', 'sessionFactory', function (scope, sessionFactory) {
    if (sessionFactory.hasValidSession()) {
      sessionFactory.redirectUser('/dashboard', false);
    }
  }]);
