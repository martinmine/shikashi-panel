/**
 * Created by marti_000 on 05.07.2015.
 */
angular.module('shikashiApp')
  .controller('ChangePasswordCtrl', ['$scope', 'userFactory', 'sessionFactory', function (scope, userFactory, sessionFactory) {
    if (!sessionFactory.hasValidSession()) {
      sessionFactory.redirectUser('/login', false);
    }

    scope.changePassword = function(currentPassword, newPassword, newPasswordConfirmed) {
      if (newPassword != newPasswordConfirmed) {
        scope.message = 'Your passwords were not the same'
      } else if (currentPassword.length < 5) {
        scope.message = 'Your password has to be at least 5 characters long';
      } else {
        userFactory.setPassword(currentPassword, newPassword, function (result) {
          scope.message = 'Password has been set';
        }, function (error) {
          scope.message = 'Wrong current password';
        });
      }
    }
  }]);
