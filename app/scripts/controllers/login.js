/**
 * Created by marti_000 on 11.06.2015.
 */
angular.module('shikashiApp')
  .controller('LoginCtrl', ['$scope', 'userFactory', 'sessionFactory', function (scope, userFactory, sessionFactory) {
    if (sessionFactory.hasValidSession()) {
      sessionFactory.redirectUser('/dashboard', false);
    }

    scope.login = function(email, password) {
      userFactory.tryLogin(email, password, function(response) {
        sessionFactory.setSessionKey(response.key);
        sessionFactory.setExpirationTime(response.expirationTime);
        sessionFactory.redirectUser('/dashboard', false);
      }, function(error) {
        switch (error.status) {
          case 400: scope.message = 'Enter a username and a password'; break;
          case 404: scope.message = 'Unknown username/password'; break;
          default: scope.message = 'Unknown server error'; break;
        }
      });
    };
  }]);
