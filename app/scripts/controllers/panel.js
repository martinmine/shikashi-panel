/**
 * Created by marti_000 on 11.06.2015.
 */
angular.module('shikashiApp')
  .controller('PanelCtrl', ['$scope', 'userFactory', 'sessionFactory', 'fileFactory', function (scope, userFactory, sessionFactory, fileFactory) {
    if (!sessionFactory.hasValidSession()) {
      sessionFactory.redirectUser('/login', false);
    }

    scope.uploads = [];
    scope.itemsPerPage = 25;
    scope.currentPage = 0;

    scope.deleteUpload = function(uploadId) {
      fileFactory.deleteUpload(uploadId, function(response) {
        scope.uploads = scope.uploads.filter(function(upload) {
          return upload.key != uploadId;
        })

      });
    };

    scope.reloadUploads = function() {
      fileFactory.getUploads(function(uploads) {
        scope.uploads = uploads.slice().reverse();
        scope.uploads.forEach(function(upload) {
          switch (upload.mimeType) {
            case 'image/png': {
              upload.extension = '.png';
              break;
            }
            case 'image/jpeg': {
              upload.extension = '.jpg';
              break;
            }
            case 'image/gif': {
              upload.extension = '.gif';
              break;
            }
            case 'video/mp4': {
              upload.extension = '.mp4';
              break;
            }
            case 'video/webm': {
              upload.extension = '.webm';
              break;
            }
            case 'application/x-zip-compressed': {
              upload.extension = '.zip';
              break;
            }
          }
        });
      });
    };

    scope.reloadUploads();

    scope.prevPage = function() {
      if (scope.currentPage > 0) {
        scope.currentPage--;
      }
    };

    scope.prevPageDisabled = function() {
      return scope.currentPage === 0 ? "disabled" : "";
    };

    scope.pageCount = function() {
      return Math.ceil(scope.uploads.length / scope.itemsPerPage) - 1;
    };

    scope.nextPage = function() {
      if (scope.currentPage < scope.pageCount()) {
        scope.currentPage++;
      }
    };

    scope.nextPageDisabled = function() {
      return scope.currentPage === scope.pageCount() ? "disabled" : "";
    };

    scope.setPage = function(page) {
      scope.currentPage = page;
    };

    scope.range = function() {
      var range = [];
      var pages = Math.ceil(scope.uploads.length / scope.itemsPerPage);

      console.log('Items. ' + pages);
      for (var i = 0; i < pages; i++) {
        range.push(i);
      }

      return range;
    };
  }]);
